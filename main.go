package main

import (
	"io"
	"net"
	"net/url"

	"github.com/gobwas/ws"
)

func server() {
	// kode diambil dari : (lower level without wsutil)
	// https://github.com/gobwas/ws/#readme
	ln, err := net.Listen("tcp", "localhost:8080")
	if err != nil {
		println(err.Error())
		return
	}
	u := ws.Upgrader{
		OnRequest: func(path []byte) error {
			println("path:", string(path))
			return nil
		},
		OnHeader: func(key, value []byte) error {
			println(
				"non-websocket header:\n",
				string(key), ":", string(value),
			)
			return nil
		},
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			// handle error
		}
		_, err = u.Upgrade(conn)
		if err != nil {
			// handle error
		}
		go func() {
			defer conn.Close()
			for {
				header, err := ws.ReadHeader(conn)
				if err != nil {
					// handle error
				}
				payload := make([]byte, header.Length)
				_, err = io.ReadFull(conn, payload)
				if err != nil {
					// handle error
				}
				if header.Masked {
					ws.Cipher(payload, header.Mask, 0)
				}
				// Reset the Masked flag, server frames must not be masked as
				// RFC6455 says.
				header.Masked = false
				if err := ws.WriteHeader(conn, header); err != nil {
					// handle error
				}
				if _, err := conn.Write(payload); err != nil {
					// handle error
				}
				if header.OpCode == ws.OpClose {
					return
				}
			}
		}()
	}
}

func client(payloadW string) (payloadR string) {
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		println(err.Error())
		return
	}
	dialer := new(ws.Dialer)
	dialer.Header = ws.HandshakeHeaderBytes(
		[]byte("Origin: http://localhost:8080\r\n"),
	)
	_, _, err = dialer.Upgrade(conn, &url.URL{
		Scheme: "ws",
		Host:   "localhost:8080",
		Path:   "/ws",
	})
	if err != nil {
		println(err.Error())
		return
	}
	err = ws.WriteFrame(conn, ws.MaskFrameInPlace(
		ws.NewTextFrame([]byte(payloadW)),
	))
	if err != nil {
		println(err.Error())
		return
	}
	frm, err := ws.ReadFrame(conn)
	if err != nil {
		println(err.Error())
		return
	}
	payloadR = string(frm.Payload)
	return
}

func main() {
	go server()

	payloadW := "test123"
	payloadR := client(payloadW)
	println(
		"payload:\n",
		"write:", payloadW, "\n",
		"read:", payloadR, "\n",
		"write == read :", payloadW == payloadR,
	)
}
